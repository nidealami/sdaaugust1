package com.sda.jpa.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter

@Entity
public class Department {
    @Id
    @Column(name = "department_id", length = 100)
    private int department_id;
    @Column(name = "name_department")
    private String name;


//manyToOne


}

