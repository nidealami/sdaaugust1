package com.sda.jpa.model;

import java.util.List;

public interface userDAO {
    Worker getByWorker_id(int workerID);

    void newWorker(Worker workerID);
    void upadeDataWorker(Worker workerID);
    void showAllWorkers(Worker workerID);
    void deleteWorker(Worker workerID);

    List<Worker> getAll();
}
