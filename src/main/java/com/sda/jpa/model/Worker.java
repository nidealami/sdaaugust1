package com.sda.jpa.model;

import com.sda.jpa.model.Department;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter

@Entity
public class Worker {
    @Id
    @Column(name = "worker_id")
    private int workerID;

    @Column(name = "first_name", length = 50)
    private char firstName;
    @Column(name = "last_name", length = 50)
    private char lastName;
    private int age;
    @Column(name = "hire_date")
    private Date hireDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "department_id", length = 100)
    private Department department_id;
//OneToMany
}
